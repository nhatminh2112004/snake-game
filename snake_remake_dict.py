import random
import copy
import greeting
from time import sleep
import os

# The initial coordinates
score = 0
head_x = 1
head_y = 9
body1_x = 2
body1_y = 9
body2_x = 3
body2_y = 9
tail_x = 4
tail_y = 9
snake_dict = {"head":[head_x,head_y], "body1":[body1_x,body1_y], "body2":[body2_x,body2_y], "tail":[tail_x,tail_y]}
part = ["head","body1","body2","tail"]
snake_dictionary = {
    "W": [snake_dict.get("head")[1] > snake_dict.get("body1")[1],1,1,-1],
    "S": [snake_dict.get("head")[1] < snake_dict.get("body1")[1],1,10,1],
    "A": [snake_dict.get("head")[0] > snake_dict.get("body1")[0],0,1,-1],
    "D": [snake_dict.get("head")[0] > snake_dict.get("body1")[0],0,18,1]
}

# The map
def mapping():
    map = [
        ["*","*","*","*","*","*","*","*","*","*","*","*","*","*","*","*","*","*","*","*"],
        ["*"," "," "," "," "," "," "," "," "," "," "," "," "," "," "," "," "," "," ","*"],
        ["*"," "," "," "," "," "," "," "," "," "," "," "," "," "," "," "," "," "," ","*"],
        ["*"," "," "," "," "," "," "," "," "," "," "," "," "," "," "," "," "," "," ","*"],
        ["*"," "," "," "," "," "," "," "," "," "," "," "," "," "," "," "," "," "," ","*"],
        ["*"," "," "," "," "," "," "," "," "," "," "," "," "," "," "," "," "," "," ","*"],
        ["*"," "," "," "," "," "," "," "," "," "," "," "," "," "," "," "," "," "," ","*"],
        ["*"," "," "," "," "," "," "," "," "," "," "," "," "," "," "," "," "," "," ","*"],
        ["*"," "," "," "," "," "," "," "," "," "," "," "," "," "," "," "," "," "," ","*"],
        ["*"," "," "," "," "," "," "," "," "," "," "," "," "," "," "," "," "," "," ","*"],
        ["*"," "," "," "," "," "," "," "," "," "," "," "," "," "," "," "," "," "," ","*"],
        ["*","*","*","*","*","*","*","*","*","*","*","*","*","*","*","*","*","*","*","*"]
    ]
    return map

# Set the position for "$"
def fruit(map,position_x,position_y):
    map[position_y][position_x] = "$"
    return map

# Draw map
def draw_map(map):
    for i in map:
            for x in i:
                print(x,end = "")
            print( )

# Draw snake
def snake(map,snake_dict):
    for i in snake_dict.values():
        map[i[1]][i[0]] = "+"
    return map

# Add the snake and fruit into the map    
def whole_thing(map,snake_dict,position_x,position_y):
    map_1 = snake(map,snake_dict)
    map_1 = fruit(map_1,position_x,position_y)
    draw_map(map_1)

# Function to move snake:
def move_snake(map,snake_dict):
    for i in snake_dict.values():
        map[i[1]][i[0]] = " "
    move = input("W or A or S or D: ")
    if move.upper() not in snake_dictionary.keys():
        raise ValueError
    else:
        if snake_dict.get("head")[snake_dictionary.get(move.upper())[1]] == snake_dictionary.get(move.upper())[2]:
            print("You've reached the wall. Go another way")
            move_snake(map,snake_dict)
        else:
            list_dict_values = list((a for a in snake_dict.values()))
            for i in range(len(list_dict_values)-1,0,-1):
                for position in [0,1]:
                    list_dict_values[i][position] = copy.copy(list_dict_values[i-1][position])
            list_dict_values[0][snake_dictionary.get(move.upper())[1]] = list_dict_values[0][snake_dictionary.get(move.upper())[1]] + snake_dictionary.get(move.upper())[-1]
            return snake_dict
   
def one_play_game(map,snake_dict,score):
    position_x = random.randint(1,10)
    position_y = random.randint(1,10)
    while snake_dict.get("head")[0] != position_x or snake_dict.get("head")[1] != position_y:
        if __name__ == "__main__":
            map = mapping()
            whole_thing(map,snake_dict,position_x,position_y)
            move_snake(map,snake_dict)
            if snake_dict.get("head")[0] == position_x and snake_dict.get("head")[1] == position_y:
                snake(map,snake_dict)
                draw_map(map)
            else: 
                whole_thing(map,snake_dict,position_x,position_y)
            os.system("cls")
    
    score += 1
    return score

def play_whole_game(map,snake_dict,score):
    score = one_play_game(map,snake_dict,score)
    print("Congrats! You've won the game.")
    choice = input("Do you wanna continue playing snake??? Press y to continue and press n to stop playing ")
    if choice.lower() == "y":
        play_whole_game(map,snake_dict,score)
    elif choice.lower() == "n":
        print("Your score is", score)
        print("Thanks for playing my games")
        sleep(5)
greeting.greeting()
input("Press enter to start the game --->")
play_whole_game(map,snake_dict,score)